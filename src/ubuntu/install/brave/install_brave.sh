#!/usr/bin/env bash
set -ex

# https://support.brave.com/hc/en-us/articles/360044860011-How-Do-I-Use-Command-Line-Flags-in-Brave-
# https://github.com/brave/brave-core/blob/9f2edc372ea22440dc382bfca758d3536dfbe022/build/commands/lib/start.js
BRAVE_ARGS="--password-store=basic --disable-brave-extension --disable-webtorrent-extension --disable-brave-rewards-extension --no-sandbox --disable-gpu --user-data-dir --no-first-run"

apt-get update
apt-get remove -y chromium-browser-l10n chromium-codecs-ffmpeg chromium-browser
apt-get install -y apt-transport-https curl gnupg
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list

apt-get update
apt-get install -y brave-browser
apt-get autoremove -y

cp /usr/share/applications/brave-browser.desktop $HOME/Desktop/
chown 1000:1000 $HOME/Desktop/brave-browser.desktop

mv /usr/bin/brave-browser /usr/bin/brave-browser-orig
cat >/usr/bin/brave-browser <<EOL
#!/usr/bin/env bash
/opt/brave.com/brave/brave ${BRAVE_ARGS} "\$@"
EOL
chmod +x /usr/bin/brave-browser
cp /usr/bin/brave-browser /usr/bin/brave

sed -i 's@exec -a "$0" "$HERE/brave" "$\@"@@g' /usr/bin/x-www-browser
cat >>/usr/bin/x-www-browser <<EOL
exec -a "\$0" "\$HERE/brave" "${BRAVE_ARGS}"  "\$@"
EOL

# Full list of policy options
# https://github.com/Prowler2/Brave-Browser-GPO-Policy/tree/master/Policy%20Templates
mkdir -p /etc/brave/policies/managed/
cat >>/etc/brave/policies/managed/default_managed_policy.json <<EOL
{"IPFSEnabled": true, "TorDisabled": false, "CommandLineFlagSecurityWarningsEnabled": false, "DefaultBrowserSettingEnabled": false}
EOL
